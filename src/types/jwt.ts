export type Jwt = {
  accessToken: string;
  expiresIn: number;
  refreshToken: string;
  tokenType: "bearer";
};

export type JwtPayload = {
  username: string;
};
