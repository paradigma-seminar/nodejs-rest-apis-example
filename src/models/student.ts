import {
  Sequelize,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  BelongsToManyAddAssociationMixin,
  BelongsToManyGetAssociationsMixin,
  BelongsToManyHasAssociationMixin,
  BelongsToManySetAssociationsMixin,
  NonAttribute,
} from "sequelize";
import { SubjectModel } from "./subject";

export interface StudentModel
  extends Model<
    InferAttributes<StudentModel>,
    InferCreationAttributes<StudentModel>
  > {
  email: string;
  firstName: string;
  lastName: string;
  registrationCode: string;
  subjects?: NonAttribute<SubjectModel[]>;
  addSubject: BelongsToManyAddAssociationMixin<SubjectModel, string>;
  getSubjects: BelongsToManyGetAssociationsMixin<SubjectModel>;
  hasSubject: BelongsToManyHasAssociationMixin<SubjectModel, string>;
  removeSubject: BelongsToManyAddAssociationMixin<SubjectModel, string>;
  setSubjects: BelongsToManySetAssociationsMixin<SubjectModel, string>;
}

export const StudentModel = (sequelize: Sequelize) => {
  return sequelize.define<StudentModel>("Student", {
    registrationCode: {
      type: DataTypes.STRING({ length: 10 }),
      primaryKey: true,
      comment: "matricola",
    },
    email: {
      type: DataTypes.STRING(60),
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    firstName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        min: 2,
        max: 50,
      },
    },
    lastName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        min: 2,
        max: 50,
      },
    },
  });
};
