import {
  Sequelize,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  CreationOptional,
  BelongsToManyAddAssociationMixin,
  BelongsToManyGetAssociationsMixin,
  BelongsToManySetAssociationsMixin,
  BelongsToManyHasAssociationMixin,
} from "sequelize";
import { StudentModel } from "./student";

export interface SubjectModel
  extends Model<
    InferAttributes<SubjectModel>,
    InferCreationAttributes<SubjectModel>
  > {
  code: string;
  credits: number;
  description: string;
  instructor: CreationOptional<string>;
  name: string;
  addStudent: BelongsToManyAddAssociationMixin<StudentModel, string>;
  getStudents: BelongsToManyGetAssociationsMixin<StudentModel>;
  hasStudent: BelongsToManyHasAssociationMixin<StudentModel, string>;
  removeStudent: BelongsToManyAddAssociationMixin<StudentModel, string>;
  setStudents: BelongsToManySetAssociationsMixin<StudentModel, string>;
}

export const SubjectModel = (sequelize: Sequelize) => {
  return sequelize.define<SubjectModel>("Subject", {
    code: {
      type: DataTypes.STRING({ length: 10 }),
      primaryKey: true,
    },
    credits: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      validate: {
        min: 1,
        max: 180,
      },
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        min: 10,
      },
    },
    instructor: {
      type: DataTypes.STRING(50),
      defaultValue: null,
      validate: {
        max: 50,
      },
      comment: "The fullname of the professor",
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        min: 3,
        max: 255,
      },
    },
  });
};
