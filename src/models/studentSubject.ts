import {
  Sequelize,
  DataTypes,
  InferCreationAttributes,
  InferAttributes,
  Model,
  CreationOptional,
} from "sequelize";
import { StudentModel } from "./student";
import { Student, Subject } from "../services/database";

interface StudentSubjectModel
  extends Model<
    InferAttributes<StudentSubjectModel>,
    InferCreationAttributes<StudentSubjectModel>
  > {
  grade: CreationOptional<number>;
  passedOn: CreationOptional<string>;
  summaCumLaude: CreationOptional<boolean>;
}

export const StudentSubjectModel = (sequelize: Sequelize) => {
  return sequelize.define<StudentSubjectModel>("StudentSubject", {
    grade: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      defaultValue: null,
      validate: {
        min: 18,
        max: 30,
      },
    },
    passedOn: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: null,
    },
    summaCumLaude: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: null,
    },
  });
};
