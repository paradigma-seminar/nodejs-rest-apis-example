import { NextFunction, Request, Response } from "express";

import { Subject } from "../services/database";

const create = async (req: Request, res: Response, next: NextFunction) => {
  const subjectProps = req.body;
  try {
    const subject = await Subject.create(subjectProps);
    return res.status(201).json(subject);
  } catch (error) {
    next(error);
  }
};

const destroy = async (req: Request, res: Response, next: NextFunction) => {
  const { subjectId } = req.params;
  try {
    const subject = await Subject.findByPk(subjectId);
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    await subject.destroy();
    return res.status(204).end();
  } catch (error) {
    next(error);
  }
};

const get = async (req: Request, res: Response, next: NextFunction) => {
  const { subjectId } = req.params;
  try {
    const subject = await Subject.findByPk(subjectId);
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    return res.status(200).json(subject);
  } catch (error) {
    next(error);
  }
};

const update = async (req: Request, res: Response, next: NextFunction) => {
  const { subjectId } = req.params;
  const { newSubjectProps } = req.body;
  const subject = await Subject.findByPk(subjectId);
  try {
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    const updatedStudent = await subject.update({
      ...subject,
      ...newSubjectProps,
    });
    return res.status(200).json(updatedStudent);
  } catch (error) {
    next(error);
  }
};

const list = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const subjects = await Subject.findAll();
    return res.json({ items: subjects });
  } catch (error) {
    next(error);
  }
};

export const subjectsController = {
  create,
  destroy,
  get,
  list,
  update,
};
