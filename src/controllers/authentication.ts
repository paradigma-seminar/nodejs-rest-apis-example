import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import ms from "ms";

import { config } from "../../config";

/**
 * Logs in the user using credentials (username and password)
 */
const login = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    const { username, password } = req.body;
    if (config.USERNAME !== username || config.PASSWORD !== password) {
      res.status(401).json({ message: "invalid credentials" });
      return;
    }

    const accessToken = jwt.sign({ username }, config.JWT_SECRET, {
      audience: config.JWT_AUDIENCE,
      issuer: config.JWT_ISSUER,
      expiresIn: config.JWT_ACCESS_DURATION,
    });
    const refreshToken = jwt.sign({ username }, config.JWT_SECRET, {
      audience: config.JWT_AUDIENCE,
      issuer: config.JWT_ISSUER,
      expiresIn: config.JWT_REFRESH_DURATION,
    });

    res.status(201).json({
      accessToken,
      refreshToken,
      expiresIn: ms(config.JWT_ACCESS_DURATION),
      tokenType: "bearer",
    });
    return;
  } catch (error) {
    next(error);
    return;
  }
};

export const authenticationController = {
  login,
};
