import { NextFunction, Request, Response } from "express";

import { Student } from "../services/database";

const create = async (req: Request, res: Response, next: NextFunction) => {
  const studentProps = req.body;
  try {
    const student = await Student.create(studentProps);
    return res.status(201).json(student);
  } catch (error) {
    next(error);
  }
};

const destroy = async (req: Request, res: Response, next: NextFunction) => {
  const { studentId } = req.params;
  try {
    const student = await Student.findByPk(studentId);
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    await student.destroy();
    return res.status(204).end();
  } catch (error) {
    next(error);
  }
};

const get = async (req: Request, res: Response, next: NextFunction) => {
  const { studentId } = req.params;
  try {
    const student = await Student.findByPk(studentId);
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    return res.status(200).json(student);
  } catch (error) {
    next(error);
  }
};

const update = async (req: Request, res: Response, next: NextFunction) => {
  const { studentId } = req.params;
  const { newStudentProps } = req.body;
  const student = await Student.findByPk(studentId);
  try {
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    const updatedStudent = await student.update({
      ...student,
      ...newStudentProps,
    });
    return res.status(200).json(updatedStudent);
  } catch (error) {
    next(error);
  }
};

const list = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const students = await Student.findAll();
    return res.json({ items: students });
  } catch (error) {
    next(error);
  }
};

export const studentController = {
  create,
  destroy,
  get,
  list,
  update,
};
