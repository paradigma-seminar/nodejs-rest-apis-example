import { NextFunction, Request, Response } from "express";

import { Student, Subject } from "../services/database";

const enrollStudent = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { studentId, subjectId } = req.params;
  try {
    const student = await Student.findByPk(studentId);
    const subject = await Subject.findByPk(subjectId);
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    await subject.addStudent(student);
    return res.status(204).end();
  } catch (error) {
    next(error);
  }
};

const setStudentGrade = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { studentId, subjectId } = req.params;
  const { grade, passedOn, summaCumLaude = false } = req.body;
  try {
    const student = await Student.findByPk(studentId);
    const subject = await Subject.findByPk(subjectId);
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    const exam = { grade, passedOn, summaCumLaude };
    await subject.setStudents([student], {
      through: exam,
    });
    return res.status(200).json(exam);
  } catch (error) {
    next(error);
  }
};

export const listStudentSubjects = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { studentId } = req.params;
  try {
    const student = await Student.findByPk(studentId);
    if (student === null) {
      return res.status(404).json({ message: "student not found" });
    }
    const subjects = await student.getSubjects();
    return res.status(200).json({ items: subjects });
  } catch (error) {
    next(error);
  }
};

export const listSubjectStudents = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { subjectId } = req.params;
  try {
    const subject = await Subject.findByPk(subjectId);
    if (subject === null) {
      return res.status(404).json({ message: "subject not found" });
    }
    const students = await subject.getStudents();
    return res.status(200).json({ items: students });
  } catch (error) {
    next(error);
  }
};

export const studentSubjectController = {
  enrollStudent,
  setStudentGrade,
  listStudentSubjects,
  listSubjectStudents,
};
