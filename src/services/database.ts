import { Sequelize } from "sequelize";

import { config } from "../../config";
import { StudentModel } from "../models/student";
import { SubjectModel } from "../models/subject";
import { StudentSubjectModel } from "../models/studentSubject";

/**
 * DB connection setup
 */
export const sequelize = new Sequelize(config.DB_CONNECTION_STRING, {
  define: {
    timestamps: false,
  },
  logging: (msg) => console.log(`[Database Service] ${msg}`),
});

export const Student = StudentModel(sequelize);
export const Subject = SubjectModel(sequelize);
export const StudentSubject = StudentSubjectModel(sequelize);

Student.belongsToMany(Subject, {
  through: StudentSubject,
  sourceKey: "registrationCode",
  targetKey: "code",
  foreignKey: "registrationCode",
});
Subject.belongsToMany(Student, {
  through: StudentSubject,
  sourceKey: "code",
  targetKey: "registrationCode",
  foreignKey: "code",
});

/**
 * The line below generates the tables if they do not exist yet
 */
sequelize.sync().then(() => console.log("DB is synced"));
