import { Router } from "express";
import { body } from "express-validator";
import { authenticationController } from "../controllers/authentication";
import { invalidType, requiredField } from "../utils/validations";
import { requestValidationMiddleware } from "../middlewares/validation";

export const router: Router = Router();

router
  .route("/login")
  .post(
    [
      body("username")
        .notEmpty()
        .withMessage(requiredField)
        .isString()
        .withMessage(invalidType),
      body("password")
        .notEmpty()
        .withMessage(requiredField)
        .isString()
        .withMessage(invalidType),
    ],
    requestValidationMiddleware,
    authenticationController.login
  );
