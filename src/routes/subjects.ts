import { Router } from "express";
import { subjectsController } from "../controllers/subjects";
import { body } from "express-validator";
import { invalidType, requiredField } from "../utils/validations";
import { studentSubjectController } from "../controllers/studentsSubjects";
import { jwtAuthenticationMiddleware } from "../middlewares/authentication";
import { requestValidationMiddleware } from "../middlewares/validation";

export const router = Router();

const subjectValidation = [
  body("code")
    .notEmpty()
    .withMessage(requiredField)
    .isString()
    .withMessage(invalidType),
  body("credits")
    .notEmpty()
    .withMessage(requiredField)
    .isInt({ min: 1, max: 180 })
    .withMessage(
      (value, meta) => `${meta.path} must be an integer in the range [1, 180]`
    ),
  body("description")
    .notEmpty()
    .withMessage(requiredField)
    .isLength({ min: 10 })
    .withMessage(
      (value, meta) =>
        `length of ${meta.path} length must be at least 10 characters`
    ),
  body("name")
    .notEmpty()
    .withMessage(requiredField)
    .isLength({ min: 10, max: 255 })
    .withMessage(
      (value, meta) =>
        `length of ${meta.path} length must be in the range [10, 255]`
    ),
];

router.use(jwtAuthenticationMiddleware);

router
  .route("/")
  .post(
    subjectValidation,
    requestValidationMiddleware,
    subjectsController.create
  )
  .get(subjectsController.list);

router
  .route("/:subjectId")
  .get(subjectsController.get)
  .put(
    subjectValidation,
    requestValidationMiddleware,
    subjectsController.update
  )
  .delete(subjectsController.destroy);

router
  .route("/:subjectId/students")
  .get(studentSubjectController.listSubjectStudents);
