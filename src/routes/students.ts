import { Router } from "express";
import { studentController } from "../controllers/students";
import { studentSubjectController } from "../controllers/studentsSubjects";
import { body } from "express-validator";
import { invalidType, requiredField } from "../utils/validations";
import { jwtAuthenticationMiddleware } from "../middlewares/authentication";
import { requestValidationMiddleware } from "../middlewares/validation";

export const router = Router();

const studentValidation = [
  body("registrationCode")
    .notEmpty()
    .withMessage(requiredField)
    .isString()
    .withMessage(invalidType),
  body("email")
    .notEmpty()
    .withMessage(requiredField)
    .isEmail()
    .withMessage((value, meta) => `${meta.path} must is not a valid email`),
  body("firstName")
    .notEmpty()
    .withMessage(requiredField)
    .isLength({ min: 2, max: 50 })
    .withMessage(
      (value, meta) => `length of ${meta.path} must be in the range [2, 50]`
    ),
  body("lastName")
    .notEmpty()
    .withMessage(requiredField)
    .isLength({ min: 2, max: 50 })
    .withMessage(
      (value, meta) => `length of ${meta.path} must be in the range [2, 50]`
    ),
];

router.use(jwtAuthenticationMiddleware);

router
  .route("/")
  .post(
    studentValidation,
    requestValidationMiddleware,
    studentController.create
  )
  .get(studentController.list);

router
  .route("/:studentId")
  .get(studentController.get)
  .put(studentValidation, requestValidationMiddleware, studentController.update)
  .delete(studentController.destroy);

router
  .route("/:studentId/subjects")
  .get(studentSubjectController.listStudentSubjects);

router
  .route("/:studentId/subjects/:subjectId")
  .post(studentSubjectController.enrollStudent)
  .put(studentSubjectController.setStudentGrade);
