import { Meta } from "express-validator";

export const requiredField = (_value: unknown, meta: Meta) =>
  `'${meta.path}' is required`;

export const invalidType = (_value: unknown, meta: Meta) =>
  `type of '${meta.path}' is invalid`;
