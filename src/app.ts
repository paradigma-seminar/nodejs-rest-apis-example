import * as bodyParser from "body-parser";
import express, { Application, NextFunction, Request, Response } from "express";
import fs from "fs";
import path from "path";
import swaggerUi from "swagger-ui-express";
import YAML from "yaml";

import { router as authRouter } from "./routes/auth";
import { router as studentsRouter } from "./routes/students";
import { router as subjectsRouter } from "./routes/subjects";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (_: Request, res: Response) =>
  res.json({ message: "The server is up and running" })
);

app.use("/static", express.static(path.join(__dirname, "..", "public")));

async function registerApiDocEndpoint(app: Application) {
  const swaggerDocPath = path.join(__dirname, "..", "public", "openapi.yaml");
  const jsonObject = YAML.parse(fs.readFileSync(swaggerDocPath, "utf8"));
  app.use("/openapi", swaggerUi.serve, swaggerUi.setup(jsonObject));
}

registerApiDocEndpoint(app).catch((err) => {
  console.error("Error mounting openapi documentation endpoint.", err);
});

app.use("/auth", authRouter);
app.use("/students", studentsRouter);
app.use("/subjects", subjectsRouter);

// 404 error handler (endpoint not found)
app.use((_: Request, res: Response) => {
  if (res.headersSent) {
    // If we got here, it means that a response has been already sent to the client,
    // hence this it not a client 404 error. In this case we do nothing.
    return;
  }
  res.status(404).json({ message: "Endpoint not found" });
});

// 500 error handler
app.use((err: Error, req: Request, res: Response, _next: NextFunction) => {
  console.error(err);
  res.status(500).json({ message: "internal server error" });
});

export default app;
