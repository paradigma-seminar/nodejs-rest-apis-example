import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

export function requestValidationMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    res.status(422).json({
      errors: validationErrors.array(),
    });
    return;
  }
  next();
}
