import { Request, Response, NextFunction } from "express";
import jwt, { JsonWebTokenError, JwtPayload } from "jsonwebtoken";

import { config } from "../../config";

export function jwtAuthenticationMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const token = getToken(req);
  if (token === undefined) {
    const responseBody = {
      message: "Authorization header is missing or has an invalid value",
    };
    res.status(401).json(responseBody);
    return;
  }

  try {
    verifyJwt(token);
  } catch (error) {
    res.status(401).json({ message: String(error) });
    return;
  }
  next();
  return;
}

/**
 * Gets the JWT from the `Authorization` request header with the `Bearer` strategy
 */
function getToken(req: Request): string | undefined {
  const authorizationHeader = req.headers.authorization;
  if (!authorizationHeader) {
    return;
  }
  const authHeaderRegex = /^Bearer (.+)/;
  const match = authHeaderRegex.exec(authorizationHeader);

  if (!match) {
    return;
  }

  return match[0];
}

/**
 * Verifies the JWT and returns its payload
 * @throws TokenExpiredError | JsonWebTokenError
 */
function verifyJwt(token: string): string | JwtPayload {
  const verificationResult = jwt.verify(token, config.JWT_SECRET, {
    issuer: config.JWT_ISSUER,
    audience: config.JWT_AUDIENCE,
  });
  if (typeof verificationResult === "string") {
    throw new JsonWebTokenError("invalid token");
  }
  return verificationResult;
}
