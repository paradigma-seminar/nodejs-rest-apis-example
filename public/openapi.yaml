openapi: 3.0.0
info:
  contact:
    email: info@paradigma.me
  description: |
    Simple REST APIs to manage subjects and students of a university course.

    Some useful links to the technologies and libraries used to build the application:
    - [Node.js](https://nodejs.org/)
    - [Typescript](https://www.typescriptlang.org/)
    - [Express](https://expressjs.com/)
    - [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
    - [Sequelize](https://sequelize.org/)
    - [Swagger](https://swagger.io/)
  license:
    name: MIT
    url: http://opensource.org/licenses/mit-license.php
  title: Node.js REST APIs example
  version: 1.0.0
servers:
  - url: "http://localhost:3000"
    description: Local development environment
tags:
  - name: auth
    description: Operations about user authentication
  - name: student
    description: Operations about students
  - name: subject
    description: Operations about subjects
  - name: documentation
    description: Operations about swagger documentation
paths:
  /auth/login:
    post:
      operationId: login
      summary: Logs the user in
      description: Generate the JWTs for the user matching the provided credentials.
      tags:
        - "auth"
      security: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/UserCredentials"
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Jwt"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "422":
          $ref: "#/components/responses/InvalidInput"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /students:
    post:
      operationId: createStudent
      summary: Creates a student
      tags:
        - "student"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Student"
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Student"
          description: successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "422":
          $ref: "#/components/responses/InvalidInput"
        "500":
          $ref: "#/components/responses/InternalServerError"
    get:
      operationId: listStudents
      summary: Lists all the students
      tags:
        - "student"
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  items:
                    type: array
                    items:
                      $ref: "#/components/schemas/Student"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /students/{studentId}:
    parameters:
      - $ref: "#/components/parameters/studentId"
    get:
      operationId: getStudent
      summary: Gets a student
      tags:
        - "student"
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Student"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
    put:
      operationId: updateStudent
      summary: Updates a student
      tags:
        - "student"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Student"
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Student"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "422":
          $ref: "#/components/responses/InvalidInput"
        "500":
          $ref: "#/components/responses/InternalServerError"
    delete:
      operationId: deleteStudent
      summary: Deletes a student
      tags:
        - "student"
      responses:
        "204":
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /students/{studentId}/subjects:
    get:
      operationId: listStudentSubjects
      summary: Lists the subjects in which a student is enrolled.
      tags:
        - "student"
      parameters:
        - $ref: "#/components/parameters/studentId"
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  items:
                    type: array
                    items:
                      $ref: "#/components/schemas/Subject"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /students/{studentId}/subjects/{subjectId}:
    post:
      operationId: enrollStudent
      summary: Enrolls a student in a subject
      tags:
        - "student"
      parameters:
        - $ref: "#/components/parameters/studentId"
        - $ref: "#/components/parameters/subjectId"
      responses:
        "204":
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
    put:
      operationId: setStudentGrade
      summary: Sets the grade of a student in a subject
      tags:
        - "student"
      parameters:
        - $ref: "#/components/parameters/studentId"
        - $ref: "#/components/parameters/subjectId"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Grade"
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  items:
                    type: array
                    items:
                      $ref: "#/components/schemas/Grade"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /subjects:
    post:
      operationId: createSubject
      summary: Creates a subject
      tags:
        - "subject"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Subject"
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Subject"
          description: successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "422":
          $ref: "#/components/responses/InvalidInput"
        "500":
          $ref: "#/components/responses/InternalServerError"
    get:
      operationId: listSubjects
      summary: Lists all the subjects
      tags:
        - "subject"
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  items:
                    type: array
                    items:
                      $ref: "#/components/schemas/Subject"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /subjects/{subjectId}:
    parameters:
      - $ref: "#/components/parameters/subjectId"
    get:
      operationId: getSubject
      summary: Gets a subject
      tags:
        - "subject"
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Subject"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
    put:
      operationId: updateSubject
      summary: Updates a subject
      tags:
        - "subject"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Subject"
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Subject"
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "422":
          $ref: "#/components/responses/InvalidInput"
        "500":
          $ref: "#/components/responses/InternalServerError"
    delete:
      operationId: deleteSubject
      summary: Deletes a subject
      tags:
        - "subject"
      responses:
        "204":
          description: Successful operation
        "401":
          $ref: "#/components/responses/Unauthorized"
        "404":
          $ref: "#/components/responses/NotFound"
        "500":
          $ref: "#/components/responses/InternalServerError"
  /openapi:
    get:
      operationId: getOpenapiUi
      summary: Gets the OAS documentation
      description: Serves this swagger API documentation page.
      tags:
        - "documentation"
      responses:
        "200":
          content:
            text/html:
              schema:
                type: string
          description: Successful operation
  /static/openapi.yaml:
    get:
      operationId: getOpenapi
      summary: Gets the OAS file
      description: Serves the swagger file.
      tags:
        - "documentation"
      responses:
        "200":
          content:
            text/yaml:
              schema:
                type: string
          description: Successful operation
components:
  parameters:
    studentId:
      in: path
      name: studentId
      required: true
      schema:
        type: string
    subjectId:
      in: path
      name: subjectId
      required: true
      schema:
        type: string
  responses:
    Unauthorized:
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
            required:
              - message
      description: No valid JWT provided
    NotFound:
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/ErrorResponseBody"
      description: Resource not found
    InvalidInput:
      content:
        application/json:
          schema:
            type: object
            properties:
              items:
                type: array
                items:
                  $ref: "#/components/schemas/ValidationError"
      description: Invalid input
    InternalServerError:
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/ErrorResponseBody"
      description: Internal Server Error
  schemas:
    ErrorResponseBody:
      type: object
      properties:
        message:
          type: string
      required:
        - message
    Grade:
      type: object
      properties:
        grade:
          type: integer
        passedOn:
          type: string
          format: date
        summaCumLaude:
          type: boolean
    Jwt:
      properties:
        accessToken:
          description: The token to be used as a value for the bearer authorization
          type: string
        expiresIn:
          description: >-
            The validity duration of the access token, after which it expires.
            Its values is expressed in milliseconds.
          type: integer
        refreshToken:
          description: >-
            The token to be used to get a new access token,

            its expiration time depends on the value provided in the
            authorization header on login request.
          type: string
        tokenType:
          description: >-
            Describes the type of authorization for which the token must be
            used, its value is always `bearer`.
          enum:
            - bearer
          type: string
      required:
        - tokenType
        - accessToken
        - expiresIn
        - refreshToken
    Student:
      type: object
      properties:
        email:
          type: string
          format: email
        firstName:
          type: string
        registrationCode: # matricola
          type: string
        lastName:
          type: string
      required:
        - email
        - firstName
        - lastName
        - registrationCode
    Subject:
      type: object
      properties:
        code:
          type: string
        credits:
          type: integer
        description:
          type: string
        instructor:
          type: string
        name:
          type: string
      required:
        - code
        - credits
        - description
        - name
    UserCredentials:
      type: object
      properties:
        password:
          type: string
        username:
          type: string
      required:
        - password
        - username
    ValidationError:
      type: object
      properties:
        location:
          type: string
        msg:
          type: string
        path:
          type: string
        type:
          type: string
      required:
        - location
        - msg
        - path
        - type
  securitySchemes:
    BearerAuth:
      bearerFormat: JWT
      scheme: bearer
      type: http
security:
  - BearerAuth: []
