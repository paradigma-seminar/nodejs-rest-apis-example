export const config = {
  DB_CONNECTION_STRING: "mysql://user:password@example.com:3306/database",
  JWT_AUDIENCE: "audience",
  JWT_ACCESS_DURATION: "1 hour",
  JWT_REFRESH_DURATION: "30 days",
  JWT_ISSUER: "issuer",
  JWT_SECRET: "secret",
  PASSWORD: "admin",
  USERNAME: "admin",
};
