# Node.js REST APIs example

A simple [Node.js](https://nodejs.org/) REST APIs implementation in [TypeScript](https://www.typescriptlang.org/), using [Express framework](https://expressjs.com/). The endpoints require a bearer token authentication with [JWT](https://jwt.io/introduction) and are validated by using [express-validator](https://express-validator.github.io/). The data are stored in a MySQL database and the app interacts with it by using the [Sequelize ORM](https://sequelize.org/).

## Requirements

### Node.js

No specific minimum version is required, but we suggest to use v18.10.0 because typings for that this version have been added to the dev dependencies in [package.json](package.json) file.

In order to install Node.js we recommend to use a Node.js version management utility. For Unix and OS X systems (and in general on any POSIX-compliant shell) we recommend the usage of [nvm](https://github.com/nvm-sh/nvm), a very popular alternative for Windows is [nvm-windows](https://github.com/coreybutler/nvm-windows).

### MySQL

The application expects the connect to a MySQL database through a connection string. The database must be empty: when the application starts, all the missing tables necessary to store the data will be created.

## Getting started

### Dependencies installation

Run `npm i` to install the required dependencies.

### Configuration settings

A [config.ts](config.ts) file exports some values which you should changes based on your environment and choices, they all are described in the table below:
| Key | Type | Description
|------------------------|------------------|------------
| `DB_CONNECTION_STRING` | string | A connection string to connect to the MySQL database, its format is mysql://user:password@domain:port/database_name.
| `JWT_AUDIENCE` | string | The value used to set the `aud` claim of a JWT.
| `JWT_ACCESS_DURATION` | string or number | The maximum allowed age for an access token to still be valid, expressed in a format suitable for [vercel/ms](https://github.com/vercel/ms).
| `JWT_REFRESH_DURATION` | string or number | The maximum allowed age for a refresh token to still be valid, expressed in a format suitable for [vercel/ms](https://github.com/vercel/ms).
| `JWT_ISSUER` | string | The value used to set the `issuer` claim of a JWT.
| `JWT_SECRET` | string | The secret used to sign the JWT with an HMAC algorithm.
| `PASSWORD` | string | The password used to authenticate the user in the login endpoint.
| `USERNAME` | string | The username used to authenticate the user in the login endpoint.

### Server start

Run `npm start` to start the application. [nodemon](https://nodemon.io/) will watch for files with the .ts and .yaml extension, so that if you make some changes to files of these types then it will restart the application.

### Server stop

In the same terminal where the command to start the server is running, use the key combination `CTRL`+`C` (or `CMD` + `C` from macOS) to stop nodemon.

## Project structure

```txt
.
├── node_modules
├── public
│   └── openapi.yaml
├── src
│   ├── controllers
│   │   ├── authentication.ts
│   │   ├── students.ts
│   │   ├── studentsSubjects.ts
│   │   └── subjects.ts
│   ├── middlewares
│   │   ├── authentication.ts
│   │   └── validation.ts
│   ├── models
│   │   ├── student.ts
│   │   ├── studentSubject.ts
│   │   └── subject.ts
│   ├── routes
│   │   ├── auth.ts
│   │   ├── student.ts
│   │   └── subject.ts
│   ├── services
│   │   └── database.ts
│   ├── types
│   │   └── jwt.ts
│   ├── utils
│   │   └── validation.ts
│   ├── app.ts
│   └── index.ts
├── .gitignore
├── config.ts
├── LICENSE
├── package.json
├── package-lock.json
├── README.md
└── tsconfig.json
```

- `node_modules` folder: contains the dependencies specified in the `package.json` file.
- `public` folder: contains the files served by the application.
- `src` folder: contains all the source code of our application.
  - `controllers` folder: contains the _controllers_, which are functions that convert the request into commands for the models, returning the response for the requested operation.
  - `middlewares` folder: contains the _middlewares_, which are functions that have access to the request object, the response object, and the next middleware function in the application's request-response cycle. See [here](https://expressjs.com/en/guide/using-middleware.html) for more details about the middlewares.
  - `models` folder: contains the _models_, which are the data structures of our application and include the logic to manipulate them.
  - `routes` folder: contains the _routers_, which are isolated instances of middlewares and routes. See [here](https://expressjs.com/en/guide/routing.html) for more details about routing.
  - `services` folder: contains the definitions of external services with which our application interacts. In this project, the only service is the database.
  - `types` folder: contains typings for data structures used within the application. In this project, it contains type definitions related to the JWTs.
  - `utils` folder: contains functions and data structures which are used in different parts of the application.
  - `app.ts`: it includes and export the definition of our server.
  - `index.ts`: it's the main file, that is the one which runs to start our application. It imports the application and attach a listener so that the serve can start handling requests from the clients.
- `.gitignore`: it lists the paths that must not be tracked by git, see [here](https://git-scm.com/docs/gitignore) for more details.
- `config.ts`: it exports the [configuration settings](#configuration-settings) described before.
- `LICENSE`: the licence under which this project is published.
- `package.json`: it describes the project with its dependencies, see [here](https://docs.npmjs.com/cli/configuring-npm/package-json) for more details.
- `package-lock.json`: it describes the exact dependencies tree generated when modifying the `node_modules` folder or the `packages.json` file, such that subsequent installs are able to generate identical trees, regardless of intermediate dependency updates. See [here](https://docs.npmjs.com/cli/configuring-npm/package-lock-json) for more details.
- `README.md`: it contains all the textual information about the project, see [here](https://markdown.land/readme-md) for more details.
- `tsconfig.json`: it contains the TypeScript configuration for the project, see [here](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html) for more details.
